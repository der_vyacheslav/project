package com.profiler;

import com.profiler.config.AppProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableConfigurationProperties(AppProperties.class)
public class ProfilerApplicationTests {

	@Test
	public void contextLoads() {
	}

}

