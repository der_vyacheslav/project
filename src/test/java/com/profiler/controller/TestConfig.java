package com.profiler.controller;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Collections;

@TestConfiguration
class TestConfig {
    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        org.springframework.security.core.userdetails.User basicUser =
                new org.springframework.security.core.userdetails.User(
                        "user@test.com",
                        "password",
                        Collections.emptyList());

        return new InMemoryUserDetailsManager(basicUser);
    }
}