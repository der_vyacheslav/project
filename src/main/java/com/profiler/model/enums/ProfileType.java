package com.profiler.model.enums;

/**
 * Social networks/messengers
 */
public enum ProfileType {
    INSTAGRAM,
    WHATSAPP,
    VIBER,
    TELEGRAM,
    FACEBOOK,
    VKONTAKTE,
    LINKEDIN,
    ;
}
