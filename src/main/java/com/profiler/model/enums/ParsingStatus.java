package com.profiler.model.enums;

/**
 * Represents current parsing status.
 */
public enum ParsingStatus {
    NEW,
    PARSING,
    TRACKED,
    STOPPED,
    ADMIN_RESTRICTED_THE_ACCESS,
    DELETED,
    ;
}
