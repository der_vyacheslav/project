package com.profiler.model;

import com.profiler.model.enums.ParsingStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Indexed
@NoArgsConstructor
@Table(name = "profiles", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"nickname"})
})
public class SocialProfile extends Profile {

    private String city;

    @Field(store = Store.YES)
    private String phoneNumber;

    @Field(store = Store.YES)
    @Column(unique = true)
    private String nickname;

    @Field
    private Boolean isBackground;

    @Field(store = Store.YES)
    private Boolean isTracked;

    @Field
    private Boolean isAddedFromBackground;

    @Embedded
    private Coordinates coordinates;

    private Integer countOfParsingAttempts;

    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
    })
    @JoinTable(name = "profiles_groups",
               joinColumns = @JoinColumn(name = "profile_id"),
               inverseJoinColumns = @JoinColumn(name = "group_id")
    )
    @IndexedEmbedded(includeEmbeddedObjectId = true, includePaths = "id")
    @Fetch(value = FetchMode.SELECT)
    private Set<Group> groups = new HashSet<>();

    @Enumerated(EnumType.STRING)
    private ParsingStatus parsingStatus;

    private Long externalUserId;

    @PrePersist
    void prePersist() {
        if (isBackground == null) {
            isBackground = Boolean.FALSE;
        }
        if (isTracked == null) {
            isTracked = Boolean.FALSE;
        }
        if (countOfParsingAttempts == null) {
            countOfParsingAttempts = 0;
        }
    }
}
