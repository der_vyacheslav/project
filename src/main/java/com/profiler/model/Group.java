package com.profiler.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.profiler.model.audit.DateAudit;
import com.profiler.model.enums.ParsingStatus;
import com.profiler.model.enums.ProfileType;
import com.profiler.util.Constants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilterFactory;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@AnalyzerDef(name = "linksAnalyzer",
        tokenizer = @TokenizerDef(factory = WhitespaceTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = ASCIIFoldingFilterFactory.class), // Replace accented characeters by their simpler counterpart (è => e, etc.)
                @TokenFilterDef(factory = LowerCaseFilterFactory.class) // Lowercase all characters
        })
@Data
@Entity
@Indexed
@NoArgsConstructor
@Table(name = "groups")
public class Group extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Field
    private String name;

    @Field
    @Analyzer(definition = "linksAnalyzer")
    private String link;

    private String avatarLink;

    @Field
    @Enumerated(EnumType.STRING)
    private ProfileType type;

    @Field
    @Enumerated(EnumType.STRING)
    private ParsingStatus parsingStatus;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime lastMessageDateTime;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "groups",
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
    })
    @ContainedIn
    private Set<SocialProfile> profiles = new HashSet<>();

    private LocalDateTime unfreezeSendDateTime;

    @PrePersist
    void preInsert() {
        if (unfreezeSendDateTime == null) {
            unfreezeSendDateTime = LocalDateTime.now();
        }
        if (parsingStatus == null) {
            parsingStatus = ParsingStatus.NEW;
        }
    }

    public void addProfiles(final Set<SocialProfile> socialProfiles) {
        socialProfiles.forEach(this::addProfile);
    }

    public void addProfile(final SocialProfile profile) {
        this.profiles.add(profile);
        profile.getGroups().add(this);
    }
}
