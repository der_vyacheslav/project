package com.profiler.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Coordinates {
    private String lat;
    private String lng;
}
