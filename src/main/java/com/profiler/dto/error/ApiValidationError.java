package com.profiler.dto.error;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

/**
 * Represents DTO for API validation errors description
 */
@Getter
@ApiModel(description = "Represents common model for API validation errors.")
final class ApiValidationError implements ApiSubError {
    @ApiModelProperty(notes = "Object on which validation error occurs.")
    private final String object;
    @ApiModelProperty(notes = "Object field on which validation error occurs.", example = "email")
    private final String field;
    @ApiModelProperty(notes = "Value which not passed validation.", example = "rejected@email.com")
    private final Object rejectedValue;
    @ApiModelProperty(notes = "Validation error description message.", example = "The email is already registered.")
    private final String message;

    ApiValidationError(String object, String message) {
        this.object = object;
        this.message = message;
        this.field = null;
        this.rejectedValue = null;
    }

    ApiValidationError(String object, String field, Object rejectedValue, String message) {
        this.object = object;
        this.field = field;
        this.rejectedValue = rejectedValue;
        this.message = message;
    }
}
