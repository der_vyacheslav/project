package com.profiler.dto.error;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.profiler.model.enums.CustomError;
import com.profiler.util.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.ConstraintViolation;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Represents DTO for API errors
 */
@Getter
@ApiModel(description = "Represents common model for API errors.")
public class ApiError {
    /** HTTP status */
    @JsonIgnore
    private HttpStatus httpStatus;
    @ApiModelProperty(notes = "HTTP status code.", example = "400")
    private int status;
    /** Unique error code */
    @ApiModelProperty(notes = "HTTP error status code description.", example = "Bad Request")
    private String error;
    @ApiModelProperty(notes = "Datetime when error occurs.", example = "dd-MM-yyyy hh:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime timestamp = LocalDateTime.now();
    @ApiModelProperty(notes = "Error description message.", example = "The email is already registered.")
    private String message;
    @ApiModelProperty(notes = "Custom error code.", example = "228")
    private CustomError customErrorCode;
    @ApiModelProperty(notes = "Validation error details.")
    private List<ApiValidationError> details;

    public ApiError(HttpStatus status) {
        this.httpStatus = status;
        this.status = status.value();
        this.error = status.getReasonPhrase();
    }

    public ApiError(HttpStatus status, Throwable ex) {
        this.httpStatus = status;
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(HttpStatus status, String message) {
        this.httpStatus = status;
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(HttpStatus status, String message, Throwable ex) {
        this.httpStatus = status;
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(HttpStatus status, String message, CustomError customError, Throwable ex) {
        this.httpStatus = status;
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.message = message;
        this.customErrorCode = customError;
        this.debugMessage = ex.getLocalizedMessage();
    }

    private void addSubError(ApiValidationError subError) {
        if (details == null) {
            details = new ArrayList<>();
        }
        details.add(subError);
    }

    private void addValidationError(String object, String field, Object rejectedValue, String message) {
        addSubError(new ApiValidationError(object, field, rejectedValue, message));
    }

    private void addValidationError(String object, String message) {
        addSubError(new ApiValidationError(object, message));
    }

    private void addValidationError(FieldError fieldError) {
        this.addValidationError(
                fieldError.getObjectName(),
                fieldError.getField(),
                fieldError.getRejectedValue(),
                fieldError.getDefaultMessage());
    }

    public void addValidationErrors(List<FieldError> fieldErrors) {
        fieldErrors.forEach(this::addValidationError);
    }

    private void addValidationError(ObjectError objectError) {
        this.addValidationError(
                objectError.getObjectName(),
                objectError.getDefaultMessage());
    }

    public void addValidationError(List<ObjectError> globalErrors) {
        globalErrors.forEach(this::addValidationError);
    }

    /**
     * Utility method for adding error of ConstraintViolation. Usually when a @Validated validation fails.
     *
     * @param cv the ConstraintViolation
     */
    private void addValidationError(ConstraintViolation<?> cv) {
        this.addValidationError(
                cv.getRootBeanClass().getSimpleName(),
                ((PathImpl) cv.getPropertyPath()).getLeafNode().asString(),
                cv.getInvalidValue(),
                cv.getMessage());
    }

    public void addValidationErrors(Set<ConstraintViolation<?>> constraintViolations) {
        constraintViolations.forEach(this::addValidationError);
    }
}
