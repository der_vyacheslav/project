package com.profiler.dto.search;

/**
 * Represents existing search types
 */
public enum SearchType {
    INTERNAL, //for search in internal database
    EXTERNAL, //for crawlers search
    ;
}
