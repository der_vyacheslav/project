package com.profiler.dto.search.profile;

import com.profiler.model.enums.ProfileType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.Synchronized;
import org.springframework.data.domain.Pageable;
import org.springframework.util.SerializationUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;

@Data
@ApiModel(description = "Represents model for search request.")
public class SearchRequestDto implements Serializable {
    @ApiModelProperty(notes = "Phone number for profiles internal search.", example = "380509379992")
    private String phoneNumber;
    @ApiModelProperty(notes = "Country for profiles internal search.", example = "Uganda")
    private String country;
    @ApiModelProperty(notes = "Initials for profiles internal search.", example = "Alex Shlyapik")
    private String initials;
    private String facebookLink;
    private String vkontakteLink;
    private String instagramLink;
    private String linkedinLink;
    private String telegramLink;
    @ApiModelProperty(notes = "The type of social network/messenger.", example = "TELEGRAM", required = true)
    private ProfileType type;
    private Pageable pageable;
    private Boolean isBackground;
    private Boolean isAddedFromBackground;

    public static SearchRequestDto getBackgroundSearchTemplate(final ProfileType type) {
        final SearchRequestDto backgroundSearchTemplate = new SearchRequestDto();
        backgroundSearchTemplate.setType(type);
        backgroundSearchTemplate.setIsBackground(Boolean.TRUE);
        return backgroundSearchTemplate;
    }

    public static SearchRequestDto clone(final SearchRequestDto search) {
        return (SearchRequestDto) SerializationUtils.deserialize(SerializationUtils.serialize(search));
    }
}
