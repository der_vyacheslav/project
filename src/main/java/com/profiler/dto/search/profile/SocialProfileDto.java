package com.profiler.dto.search.profile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.profiler.model.enums.ProfileType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(value = {"id"}, ignoreUnknown = true, allowGetters = true)
public class SocialProfileDto {
    private Long id;
    private String initials;
    private String phoneNumber;
    private String country;
    private String city;
    private ProfileType type;
    private String link;
    private String nickname;
    private String avatarLink;
    private String birthDate;
    private Boolean isBackground;
    private Boolean isTracked;
    private Boolean isAddedFromBackground;
    private String lat;
    private String lng;
    private Long messagesCount;
}
