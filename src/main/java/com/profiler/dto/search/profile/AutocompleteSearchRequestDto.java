package com.profiler.dto.search.profile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Pageable;

@Data
@ApiModel(description = "Represents model for autocomplete search request.")
public class AutocompleteSearchRequestDto {
    @ApiModelProperty(notes = "Profile initials parts for autocomplete search.", example = "Joh Smi")
    private String initialsParts;
    @ApiModelProperty(notes = "Group ID for search in.", example = "7", required = true)
    private Long groupId;

    private Pageable pageable;
}
