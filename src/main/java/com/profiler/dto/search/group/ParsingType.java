package com.profiler.dto.search.group;

/**
 * Represents enums for different groups parsing actions.
 */
public enum ParsingType {
    SEARCH, //existence check
    PARSE, //parsing groups internal data
    ;
}
