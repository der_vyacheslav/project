package com.profiler.dto.search.group;

import com.profiler.dto.search.SearchType;
import com.profiler.model.enums.ProfileType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.SerializationUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(description = "Represents model for groups search request.")
public class GroupSearchRequestDto implements Serializable {
    @ApiModelProperty(notes = "Group name.", example = "Terrified group")
    private String name;
    @ApiModelProperty(notes = "WhatsApp groups links list.")
    private List<String> whatsappLinks;
    @ApiModelProperty(notes = "Viber groups links list.")
    private List<String> viberLinks;
    @ApiModelProperty(notes = "Telegram groups links list.")
    private List<String> telegramLinks;
    @ApiModelProperty(notes = "The type of group.")
    private ProfileType type;
    @ApiModelProperty(notes = "The search type.")
    private SearchType searchType;

    private Pageable pageable;

    public static GroupSearchRequestDto clone(final GroupSearchRequestDto search) {
        return (GroupSearchRequestDto) SerializationUtils.deserialize(SerializationUtils.serialize(search));
    }
}
