package com.profiler.dto.search.group;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.profiler.model.enums.ProfileType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Represents DTO for groups parsing requests.
 */
@Data
@ApiModel(description = "Represents model for groups parsing request.")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupParserRequestDto {
    @ApiModelProperty(notes = "Groups links for parse.", example = "https://invite.viber.com/?g2=5e172b8d74ac30357c46ef80,https://invite.viber.com/?g2=5e172b8dfd6a46deeb9280fa")
    private List<String> value;
    @ApiModelProperty(notes = "Messenger/social network name.", example = "TELEGRAM")
    private ProfileType type;
    @ApiModelProperty(notes = "Last parsed external message id.", example = "9379992, 322")
    private List<Long> lastMessageId;
}