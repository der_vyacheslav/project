package com.profiler.dto.search.group;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.profiler.model.enums.ParsingStatus;
import com.profiler.model.enums.ProfileType;
import com.profiler.util.Constants;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ApiModel(description = "Represents DTO for group.")
public class GroupDto {
    private Long id;
    private String name;
    private String link;
    @JsonAlias("photo")
    private String avatarLink;
    private ProfileType type;
    private ParsingStatus parsingStatus;
    private Long totalMembersCount;
    private Long trackedMembersCount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime lastMessageDateTime;
    private Long messagesCount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime firstMessageDateTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime unfreezeSendDateTime = LocalDateTime.now().plusHours(1L);
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime lastSearchMessageDateTime;
}
