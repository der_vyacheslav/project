package com.profiler.dto.search;

import com.profiler.dto.search.profile.SearchCountResponseDto;
import com.profiler.model.enums.ProfileType;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Factory class for different {@link com.profiler.dto.search.profile.SearchCountResponseDto} creation.
 */
public final class SearchCountFactory {
    private static final Map<ProfileType, SearchCountResponseDto> EMPTY_COUNTS = new HashMap<ProfileType, SearchCountResponseDto>() {{
        EnumSet.allOf(ProfileType.class).forEach(type -> {
            put(type, SearchCountResponseDto
                                .builder()
                                .type(type)
                                .count(0)
                                .build());
        });
    }};

    public static SearchCountResponseDto getEmptyCount(final ProfileType type) {
        return EMPTY_COUNTS.get(type);
    }

    public static List<SearchCountResponseDto> getEmptyCounts() {
        return new ArrayList<>(EMPTY_COUNTS.values());
    }
}
