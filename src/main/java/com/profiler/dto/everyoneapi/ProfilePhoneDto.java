package com.profiler.dto.everyoneapi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Represents DTO class for external profile initials by phone number.
 */
@Data
@Builder
@ApiModel(description = "Represents model for external profile initials search by phone number.")
public class ProfilePhoneDto {

    @ApiModelProperty(notes = "Found profile initials by given phone number.", example = "Alex Shlyapik")
    private String initials;

    @ApiModelProperty(notes = "Shows is profile initials found by given phone number.", required = true)
    private Boolean isExists;
}
