package com.profiler.dto.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.profiler.validator.ValidLoginCredentials;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Represents DTO class for user's login request data
 */
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Represents model for log in request.")
@ValidLoginCredentials
public final class LoginRequest {
    @NotBlank
    @Email
    @ApiModelProperty(notes = "Unique email of the user.", example = "alex.shlyapik@gmail.com", required = true)
    private String email;

    @NotBlank
    @ApiModelProperty(notes = "Password for account entering.", example = "secured1GLEK", required = true)
    private String password;

    public LoginRequest(@NotBlank @Email final String email, @NotBlank final String password) {
        this.email = email;
        this.password = password;
    }
}
