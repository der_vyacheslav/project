package com.profiler.dto.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents DTO class for user's authentication info data
 */
@Getter
@AllArgsConstructor
@ApiModel(description = "Represents common model for successful auth request.")
public final class AuthResponse {
    @JsonProperty("access_token")
    @ApiModelProperty(notes = "Access token which have to be sent in \"Authorization:\" request header.")
    private final String accessToken;

    @JsonProperty("token_type")
    @ApiModelProperty(notes = "Access token type, which have to be sent in \"Authorization:\" request header before accessToken itself",
            example = "Bearer ")
    private final String tokenType;

    @JsonProperty("expires_in")
    @ApiModelProperty(notes = "Represents time in millis after which accessToken will expire.", example = "86400000")
    private final long expiresIn;
}
