package com.profiler.dto.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.profiler.validator.ValidPassword;
import com.profiler.validator.ValidSecureCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Represents DTO class for user's sign up request data
 */
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Represents model for new user sign up request.")
public final class SignUpRequest {
    @NotBlank
    @Email
    @ApiModelProperty(notes = "Unique email of the user.", example = "alex.shlyapik@gmail.com", required = true)
    private String email;

    @NotBlank
    @ValidPassword
    @ApiModelProperty(notes = "Password for account entering.", example = "secured1GLEK", required = true)
    private String password;

    @ValidSecureCode
    @ApiModelProperty(notes = "Secure code for company admin registration.", example = "123e4567-e89b-12d3-a456-426655440000")
    private String secureCode;

    public SignUpRequest(@NotBlank @Email String email, @NotBlank String password) {
        this.email = email;
        this.password = password;
    }

    public SignUpRequest(@NotBlank @Email String email, @NotBlank String password, String secureCode) {
        this.email = email;
        this.password = password;
        this.secureCode = secureCode;
    }
}
