package com.profiler.mapper;

import com.profiler.dto.search.group.GroupDto;
import com.profiler.model.Group;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL,
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GroupMapper {

    GroupDto toDto(Group group);

    Group toEntity(GroupDto dto);

    List<GroupDto> toDtos(List<Group> groups);

    List<Group> toEntities(List<GroupDto> dtos);
}
