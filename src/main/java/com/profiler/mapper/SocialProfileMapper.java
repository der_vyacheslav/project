package com.profiler.mapper;

import com.profiler.dto.search.profile.SocialProfileDto;
import com.profiler.model.SocialProfile;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.springframework.util.StringUtils;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL,
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SocialProfileMapper {
    String PHONE_REGEX = "(\\d{1})(\\d{3})(\\d{3})(\\d+)";
    String PHONE_FORMAT = "+$1 ($2) $3-$4";

    SocialProfileDto toDto(SocialProfile socialProfile);

    SocialProfile toEntity(SocialProfileDto dto);

    List<SocialProfileDto> toDtos(List<SocialProfile> profiles);

    List<SocialProfile> toEntities(List<SocialProfileDto> profiles);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    void update(@MappingTarget SocialProfile socialProfile, SocialProfileDto dto);

    @AfterMapping
    default void setCustomFields(SocialProfile socialProfile, @MappingTarget SocialProfileDto result) {
        if (StringUtils.hasText(socialProfile.getPhoneNumber())) {
            result.setPhoneNumber(socialProfile.getPhoneNumber().replaceFirst(PHONE_REGEX, PHONE_FORMAT));
        }
    }

    @AfterMapping
    default void setCustomFields(SocialProfileDto dto, @MappingTarget SocialProfile result) {
        if (StringUtils.hasText(dto.getPhoneNumber())) {
            result.setPhoneNumber(dto.getPhoneNumber().replace("+", ""));
        }
    }
}
