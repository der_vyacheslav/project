package com.profiler.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.profiler.dto.PageDto;
import com.profiler.dto.everyoneapi.ProfilePhoneDto;
import com.profiler.dto.search.profile.AutocompleteSearchRequestDto;
import com.profiler.dto.search.profile.ParserRequestDto;
import com.profiler.dto.search.profile.SearchCountResponseDto;
import com.profiler.dto.search.profile.SearchRequestDto;
import com.profiler.dto.search.profile.SocialProfileDto;
import com.profiler.mapper.SocialProfileMapper;
import com.profiler.model.Coordinates;
import com.profiler.model.SocialProfile;
import com.profiler.model.enums.ProfileType;
import com.profiler.service.AddressService;
import com.profiler.service.ExternalService;
import com.profiler.service.SocialProfileService;
import com.profiler.service.impl.ProfileSearchService;
import com.profiler.util.Constants;
import com.profiler.util.JsonItem;
import com.profiler.util.JsonItemFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/profiles", produces = MediaType.APPLICATION_JSON_VALUE)
@Api("Endpoints for the profiles search.")
public class SocialProfileController {
    private final SocialProfileService socialProfileService;
    private final RestTemplate restTemplate;
    private final JsonItemFactory jsonItemFactory;
    private final ExternalService externalService;
    private final ObjectMapper objectMapper;

    @GetMapping("/autocomplete")
    @Secured({Constants.Roles.ROLE_USER, Constants.Roles.ROLE_ADMIN})
    @ApiOperation("Autocomplete search by profile's initials parts.")
    public JsonItem<PageDto<SocialProfileDto>> getProfilesByInitials(@ModelAttribute final AutocompleteSearchRequestDto search,
                                                                     Pageable pageable) {
        search.setPageable(pageable);
        return jsonItemFactory.successResult(socialProfileService.autocompleteSearch(search));
    }

    @GetMapping("/search")
    @Secured({Constants.Roles.ROLE_USER, Constants.Roles.ROLE_ADMIN})
    @ApiOperation("Returns the profiles search result.")
    public JsonItem<PageDto<SocialProfileDto>> internalProfilesSearch(@ModelAttribute SearchRequestDto search,
                                                                      Pageable pageable) {
        search.setPageable(pageable);
        return jsonItemFactory.successResult(socialProfileService.internalSearch(search));
    }
}
