package com.profiler.controller;

import com.profiler.dto.PageDto;
import com.profiler.dto.search.group.GroupDto;
import com.profiler.dto.search.group.GroupParserRequestDto;
import com.profiler.dto.search.group.GroupSearchRequestDto;
import com.profiler.dto.search.message.ParsedData;
import com.profiler.dto.search.message.ParsedMessage;
import com.profiler.dto.search.message.ParsedUser;
import com.profiler.dto.search.profile.SearchCountResponseDto;
import com.profiler.model.Group;
import com.profiler.model.Message;
import com.profiler.model.SocialProfile;
import com.profiler.model.enums.ParsingStatus;
import com.profiler.model.enums.ProfileType;
import com.profiler.service.ExternalService;
import com.profiler.service.GroupService;
import com.profiler.service.MessageService;
import com.profiler.service.SocialProfileService;
import com.profiler.util.JsonItem;
import com.profiler.util.JsonItemFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Api("Endpoints for social networks/messenger groups actions.")
@RequestMapping(value = "/groups", produces = MediaType.APPLICATION_JSON_VALUE)
public class GroupController {
    private final GroupService groupService;
    private final JsonItemFactory jsonItemFactory;
    private final ExternalService externalService;
    private final SocialProfileService socialProfileService;

    @GetMapping("/{id}")
    @ApiOperation("Returns the group by given primary key.")
    public JsonItem<GroupDto> getGroupById(@ApiParam(value = "The group primary key.", required = true)
                                           @PathVariable final Long id) {
        return jsonItemFactory.successResult(groupService.getDto(id));
    }

    @GetMapping("/search")
    @ApiOperation("Returns the groups which satisfying search result.")
    public JsonItem<PageDto<GroupDto>> searchGroups(@ModelAttribute final GroupSearchRequestDto search,
                                                    Pageable pageable) {
        search.setPageable(pageable);
        return jsonItemFactory.successResult(groupService.internalSearch(search));
    }
}
