package com.profiler.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.profiler.dto.error.ApiError;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Represents common model for API responses.")
public class JsonItem<T> {

    @ApiModelProperty(notes = "Response data.")
    private T data;

    @ApiModelProperty(notes = "Response errors.")
    private ApiError error;

    JsonItem(final T data) {
        this.data = data;
    }

    public JsonItem(final ApiError error) {
        this.error = error;
    }
}
