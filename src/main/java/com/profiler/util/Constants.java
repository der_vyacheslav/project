package com.profiler.util;

import java.math.BigDecimal;

/**
 * Represents application constants
 */
public final class Constants {
    /** Number constants */
    public static class Numbers {
        public static final BigDecimal EMPTY = BigDecimal.ZERO;

        /** Suppress default constructor for noninstantiability */
        private Numbers() {}
    }

    public static class Roles {
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_OWNER = "ROLE_OWNER";
        public static final String ROLE_MANAGER = "ROLE_MANAGER";
        public static final String ROLE_VISITOR = "ROLE_VISITOR";

        /** Suppress default constructor for noninstantiability */
        private Roles() {}
    }

    public static class Telegram {
        public static final String NICKNAME_PREFIX = "@";
        public static final String GROUP_PREFIX = "https://t.me/";

        /** Suppress default constructor for noninstantiability */
        private Telegram() {}
    }

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "dd-MM-yyyy";
}
