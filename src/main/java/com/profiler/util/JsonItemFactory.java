package com.profiler.util;

import com.profiler.dto.error.ApiError;
import com.profiler.exception.ApiExceptionCode;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class JsonItemFactory {

    public <T> JsonItem<T> successResult(final T data) {
        return new JsonItem<>(data);
    }

    public <T> JsonItem<T> failResult(ApiError error) {
        return new JsonItem<>(error);
    }

    public <T> JsonItem<T> failResult(ApiExceptionCode code) {
        return failResult(String.valueOf(code.getCode()), code.getStatus(), code.getDefaultMessage());
    }

    private <T> JsonItem<T> failResult(final String code, final HttpStatus status, final String message) {
        final ApiError error = new ApiError(status);

        return new JsonItem<>(error);
    }
}