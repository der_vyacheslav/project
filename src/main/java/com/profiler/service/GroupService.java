package com.profiler.service;

import com.profiler.dto.PageDto;
import com.profiler.dto.search.group.GroupDto;
import com.profiler.dto.search.group.GroupSearchRequestDto;
import com.profiler.dto.search.message.AdminMessageDto;
import com.profiler.dto.search.profile.SearchCountResponseDto;
import com.profiler.model.Group;
import com.profiler.model.enums.ParsingStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface GroupService extends GenericService<Group, Long> {

    PageDto<GroupDto> internalSearch(GroupSearchRequestDto search);

    List<SearchCountResponseDto> searchCount(GroupSearchRequestDto search);

    GroupDto getDto(Long id);

    List<Group> getByParsingStatusIn(List<Long> ids, Set<ParsingStatus> parsingStatuses);

    void updateGroupsStatus(List<Long> ids, ParsingStatus status);
}
