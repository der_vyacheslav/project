package com.profiler.service;

import com.profiler.dto.everyoneapi.ProfilePhoneDto;
import com.profiler.dto.search.group.GroupDto;
import com.profiler.dto.search.group.GroupParserRequestDto;
import com.profiler.dto.search.message.ParsedData;
import com.profiler.dto.search.profile.ParserRequestDto;
import com.profiler.dto.search.profile.SocialProfileDto;
import com.profiler.model.Coordinates;

import java.util.List;

public interface ExternalService {
    ProfilePhoneDto getProfileInitialsByPhoneNumber(String phoneNumber);

    Coordinates getCoordinatesByAddress(String country, String city, String address);
}
