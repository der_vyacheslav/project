package com.profiler.service;

import com.profiler.dto.PageDto;
import com.profiler.dto.search.profile.AutocompleteSearchRequestDto;
import com.profiler.dto.search.profile.SearchCountResponseDto;
import com.profiler.dto.search.profile.SearchRequestDto;
import com.profiler.dto.search.profile.SocialProfileDto;
import com.profiler.model.Group;
import com.profiler.model.SocialProfile;
import com.profiler.model.enums.ParsingStatus;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface SocialProfileService extends GenericService<SocialProfile, Long> {

    PageDto<SocialProfileDto> internalSearch(SearchRequestDto searchRequestDto);

    PageDto<SocialProfileDto> autocompleteSearch(AutocompleteSearchRequestDto search);
}
