package com.profiler.service.impl;

import com.profiler.dto.search.profile.AutocompleteSearchRequestDto;
import com.profiler.dto.search.profile.SearchRequestDto;
import com.profiler.model.SocialProfile;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;

@Service
@Transactional
public class ProfileSearchService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileSearchService.class);

    private static final String MOCK_FOR_EMPTY_FIELD_VALUE= "_M0cK|v@LuE1";

    private final EntityManager entityManager;

    @Autowired
    public ProfileSearchService(final EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public void clearEntityManager() {
        entityManager.clear();
    }

    public int internalSearchCount(final SearchRequestDto search) {
        LOGGER.info("Internal search with type: {}", search.getType());
        final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        final Query luceneQuery = getLuceneQuery(search, fullTextEntityManager);

        return fullTextEntityManager.createFullTextQuery(luceneQuery, SocialProfile.class).getResultSize();
    }

    @SuppressWarnings("unchecked")
    public Page<SocialProfile> fuzzySearch(final SearchRequestDto search) {
        final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        final Query luceneQuery = getLuceneQuery(search, fullTextEntityManager);

        final FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, SocialProfile.class)
                .setFirstResult(search.getPageable().getPageNumber() * search.getPageable().getPageSize())
                .setMaxResults(search.getPageable().getPageSize());
        fullTextQuery.setProjection(FullTextQuery.SCORE, FullTextQuery.EXPLANATION, FullTextQuery.THIS);

        final List resultList = fullTextQuery.getResultList();
        return new PageImpl<>(resultList, search.getPageable(), fullTextQuery.getResultSize());
    }

    @SuppressWarnings("unchecked")
    public Page<SocialProfile> autocompleteSearch(final AutocompleteSearchRequestDto search) {
        final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        final Query luceneQuery = getAutocompleteLuceneQuery(search, fullTextEntityManager);

        final FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, SocialProfile.class)
                .setFirstResult(search.getPageable().getPageNumber() * search.getPageable().getPageSize())
                .setMaxResults(search.getPageable().getPageSize());

        return new PageImpl<>(fullTextQuery.getResultList(), search.getPageable(), fullTextQuery.getResultSize());
    }

    private Query getAutocompleteLuceneQuery(final AutocompleteSearchRequestDto search, final FullTextEntityManager fullTextEntityManager) {
        final QueryBuilder queryBuilder = getQueryBuilder(fullTextEntityManager);
        final BooleanJunction resultJunction = queryBuilder.bool();
        if (search.getGroupId() != null) {
            resultJunction
                    .must(queryBuilder
                            .keyword()
                            .onField("groups.id")
                            .matching(search.getGroupId())
                            .createQuery());
        }
        if (StringUtils.hasText(search.getInitialsParts())) {
            resultJunction
                    .must(queryBuilder
                            .keyword()
                            .onField("initials")
                            .matching(search.getInitialsParts())
                            .createQuery());
        }
        return resultJunction.createQuery();
    }

    private Query getLuceneQuery(final SearchRequestDto search, final FullTextEntityManager fullTextEntityManager) {
        final QueryBuilder queryBuilder = getQueryBuilder(fullTextEntityManager);

        final BooleanJunction resultJunction = queryBuilder.bool();
        final BooleanJunction initialsJunction = queryBuilder.bool();

        if (hasText(search.getInitials())) {
            final Query lastNameFuzzyQuery = queryBuilder
                    .keyword()
                    .fuzzy()
                    .withThreshold(.7f)
                    .withPrefixLength(1)
                    .onField("lastName").boostedTo(50f)
                    .matching(search.getInitials())
                    .createQuery();

            initialsJunction
                    .should(lastNameFuzzyQuery)
                    .boostedTo(50f);

            final Query firstNameFuzzyQuery = queryBuilder
                    .keyword()
                    .fuzzy()
                    .withThreshold(.7f)
                    .withPrefixLength(1)
                    .onField("firstName").boostedTo(0.5f)
                    .matching(search.getInitials())
                    .createQuery();

            initialsJunction
                    .should(firstNameFuzzyQuery)
                    .boostedTo(0.5f);
        }

        final Query typeQuery = queryBuilder
                .keyword()
                .onField("type")
                .matching(search.getType())
                .createQuery();

        resultJunction
                .must(typeQuery)
                .boostedTo(0.2f);

        if (search.getIsAddedFromBackground() != null) {
            resultJunction
                    .must(queryBuilder
                            .keyword()
                            .onField("isAddedFromBackground")
                            .matching(Boolean.TRUE)
                            .createQuery());
        }

        return resultJunction.createQuery();
    }

    private void mockSearchOnEmptyLink(final QueryBuilder queryBuilder, final BooleanJunction resultJunction) {
        resultJunction
                .must(queryBuilder
                        .keyword()
                        .onField("phoneNumber")
                        .matching(MOCK_FOR_EMPTY_FIELD_VALUE)
                        .createQuery());
    }

    private QueryBuilder getQueryBuilder(final FullTextEntityManager fullTextEntityManager) {
        return fullTextEntityManager.getSearchFactory()
                    .buildQueryBuilder()
                    .forEntity(SocialProfile.class)
                    .overridesForField("initials", "initialsAnalyzer")
                    .get();
    }

    private QueryBuilder getAutocompleteQueryBuilder(final FullTextEntityManager fullTextEntityManager) {
        return fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(SocialProfile.class)
                .overridesForField("initials", "initialsAnalyzer")
                .get();
    }

    @PostConstruct
    public void initializeHibernateSearch() {
        try {
            final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
            fullTextEntityManager.createIndexer().startAndWait();
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }
}
