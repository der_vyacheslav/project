package com.profiler.service.impl;

import com.profiler.dto.PageDto;
import com.profiler.dto.search.SearchCountFactory;
import com.profiler.dto.search.SearchType;
import com.profiler.dto.search.group.GroupDto;
import com.profiler.dto.search.group.GroupParserRequestDto;
import com.profiler.dto.search.group.GroupSearchRequestDto;
import com.profiler.dto.search.message.AdminMessageDto;
import com.profiler.dto.search.message.MessageDto;
import com.profiler.dto.search.message.ParsedData;
import com.profiler.dto.search.message.ParsedMessage;
import com.profiler.dto.search.message.ParsedUser;
import com.profiler.dto.search.profile.SearchCountResponseDto;
import com.profiler.exception.ResourceNotFoundException;
import com.profiler.mapper.GroupMapper;
import com.profiler.model.Group;
import com.profiler.model.Message;
import com.profiler.model.SocialProfile;
import com.profiler.model.enums.ParsingStatus;
import com.profiler.model.enums.ProfileType;
import com.profiler.repository.GroupRepository;
import com.profiler.service.ExternalService;
import com.profiler.service.GroupService;
import com.profiler.service.MessageService;
import com.profiler.service.SocialProfileService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.profiler.util.Constants.Telegram.GROUP_PREFIX;
import static com.profiler.util.Constants.Telegram.NICKNAME_PREFIX;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceImpl.class);
    private static final Set<ParsingStatus> PARSING_STATUSES = EnumSet.of(ParsingStatus.TRACKED, ParsingStatus.STOPPED);
    private static final long MESSAGE_SEND_FREEZE_HOURS = 12;

    private final GroupRepository groupRepository;
    private final GroupSearchService groupSearchService;
    private final GroupMapper groupMapper;

    @Override
    @Transactional
    public Group add(final Group group) {
        LOGGER.info("Saving group with link: {}", group.getLink());
        return groupRepository.save(group);
    }

    @Override
    @Transactional
    public Group edit(final Group group) {
        LOGGER.info("Editing group with id: {}", group.getId());
        return add(group);
    }

    @Override
    @Transactional
    public void delete(final Group group) {
        LOGGER.info("Deleting group with id: {}", group.getId());
        groupRepository.delete(group);
    }

    @Override
    public Group get(final Long id) {
        return groupRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Group", "id", id));
    }

    @Override
    public GroupDto getDto(final Long id) {
        final Group group = get(id);
        setInternalGroupData(group);
        return groupMapper.toDto(group);
    }

    @Override
    public List<Group> getAll() {
        return groupRepository.findAll();
    }

    @Override
    public PageDto<GroupDto> internalSearch(final GroupSearchRequestDto search) {
        if (!isSearchRequestValid(search)) {
            return new PageDto<>(Collections.emptyList(), Page.empty(search.getPageable()));
        }

        final Page<Group> searchResult = groupSearchService.fuzzySearch(search);
        final List<Group> content = searchResult.getContent();
        if (!CollectionUtils.isEmpty(content) && SearchType.INTERNAL.equals(search.getSearchType())) {
            content.forEach(this::setInternalGroupData);
        }

        return new PageDto<>(groupMapper.toDtos(content), searchResult);
    }

    @Override
    public List<Group> getByParsingStatusIn(final List<Long> ids, final Set<ParsingStatus> parsingStatuses) {
        return groupRepository.findByIdInAndParsingStatusIn(ids, parsingStatuses);
    }
}
