package com.profiler.service.impl;

import org.apache.lucene.search.similarities.BM25Similarity;

public class IgnoreCoordsSimilarity extends BM25Similarity {
    @Override
    public float coord(int overlap, int maxOverlap) {
        return 1f;
    }
}
