package com.profiler.service.impl;

import com.profiler.dto.search.group.GroupSearchRequestDto;
import com.profiler.model.Group;
import com.profiler.model.enums.ParsingStatus;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;
import static org.springframework.util.StringUtils.hasText;

@Service
@Transactional
public class GroupSearchService {
    private static final int NAME_FIELD_SLOP_FACTOR = 3;
    private static final int LINK_FIELD_BOOST_FACTOR = 2;

    private final EntityManager entityManager;

    @Autowired
    public GroupSearchService(final EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @SuppressWarnings("unchecked")
    public Page<Group> fuzzySearch(final GroupSearchRequestDto search) {
        final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        final Query luceneQuery = getLuceneQuery(search, fullTextEntityManager);

        final FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, Group.class)
                .setFirstResult(search.getPageable().getPageNumber() * search.getPageable().getPageSize())
                .setMaxResults(search.getPageable().getPageSize());

        return new PageImpl<>(fullTextQuery.getResultList(), search.getPageable(), fullTextQuery.getResultSize());
    }

    private Query getLuceneQuery(final GroupSearchRequestDto search, final FullTextEntityManager fullTextEntityManager) {
        final QueryBuilder queryBuilder = getQueryBuilder(fullTextEntityManager);
        final BooleanJunction resultJunction = queryBuilder.bool();

        handleNameWithLinks(search, queryBuilder, resultJunction);
        handleGroupTypeParam(search, queryBuilder, resultJunction);

        return resultJunction.createQuery();
    }

    private void handleNameWithLinks(final GroupSearchRequestDto search, final QueryBuilder queryBuilder,
                                     final BooleanJunction resultJunction) {
        final String name = search.getName();
        switch (search.getType()) {
            case WHATSAPP:
                handleNameWithLinks(queryBuilder, resultJunction, search.getWhatsappLinks(), name);
                break;
            case VIBER:
                handleNameWithLinks(queryBuilder, resultJunction, search.getViberLinks(), name);
                break;
            case TELEGRAM:
                handleNameWithLinks(queryBuilder, resultJunction, search.getTelegramLinks(), name);
                break;
        }
    }

    private void handleNameWithLinks(final QueryBuilder queryBuilder, final BooleanJunction resultJunction,
                                     final List<String> links, final String name) {
        if (!isEmpty(links) && hasText(name)) {
            final BooleanJunction linksWithNameJunction = queryBuilder.bool();
            linksWithNameJunction.should(queryBuilder
                    .keyword()
                    .onField("link")
                    .boostedTo(LINK_FIELD_BOOST_FACTOR)
                    .matching(String.join(" ", links))
                    .createQuery());
            if (hasText(name)) {
                linksWithNameJunction
                        .should(getNameMatchingQuery(queryBuilder, name));
            }
            resultJunction.must(linksWithNameJunction.createQuery());
        } else if (!isEmpty(links) && StringUtils.isEmpty(name)) {
            resultJunction.must(queryBuilder
                    .keyword()
                    .onField("link")
                    .boostedTo(LINK_FIELD_BOOST_FACTOR)
                    .matching(String.join(" ", links))
                    .createQuery());
        } else {
            if (hasText(name)) {
                resultJunction.must(getNameMatchingQuery(queryBuilder, name));
            }
        }
    }

    private void handleGroupTypeParam(final GroupSearchRequestDto search, final QueryBuilder queryBuilder,
                                      final BooleanJunction resultJunction) {
        if (search.getType() != null) {
            resultJunction
                    .must(queryBuilder
                            .keyword()
                            .onField("type")
                            .matching(search.getType())
                            .createQuery());

        }
    }

    private Query getNameMatchingQuery(final QueryBuilder queryBuilder, final String name) {
        return queryBuilder
                .phrase()
                .withSlop(NAME_FIELD_SLOP_FACTOR)
                .onField("name")
                .sentence(name)
                .createQuery();
    }

    private Query getParsingStatusMatchingQuery(final QueryBuilder queryBuilder, final ParsingStatus parsingStatus) {
        return queryBuilder
                .keyword()
                .onField("parsingStatus")
                .matching(parsingStatus)
                .createQuery();
    }

    private QueryBuilder getQueryBuilder(final FullTextEntityManager fullTextEntityManager) {
        return fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(Group.class)
                .get();
    }
}
