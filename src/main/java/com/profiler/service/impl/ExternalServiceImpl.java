package com.profiler.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.profiler.config.AppProperties;
import com.profiler.dto.PageDto;
import com.profiler.dto.everyoneapi.EveryOneApiResponseDto;
import com.profiler.dto.everyoneapi.ProfilePhoneDto;
import com.profiler.dto.search.group.GroupDto;
import com.profiler.dto.search.group.GroupParserRequestDto;
import com.profiler.dto.search.message.ParsedData;
import com.profiler.dto.search.profile.ParserRequestDto;
import com.profiler.dto.search.profile.SocialProfileDto;
import com.profiler.model.Coordinates;
import com.profiler.service.ExternalService;
import com.profiler.util.JsonItem;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Represents service for external services calls.
 */
@Service
@RequiredArgsConstructor
public class ExternalServiceImpl implements ExternalService {
    private static final Logger logger = LoggerFactory.getLogger(ExternalServiceImpl.class);
    private static final String EVERYONE_API_QUERY_PARAM_NAME = "data";
    private static final String EVERYONE_API_QUERY_PARAM_VALUE = "name,expanded_name";

    private static AppProperties appProperties;
    private static GeoApiContext context;

    private final RestTemplate restTemplate;

    @Autowired
    private void setAppProperties(AppProperties appProperties) {
        ExternalServiceImpl.appProperties = appProperties;
        context = new GeoApiContext.Builder()
                .apiKey(appProperties.getCommon().getGoogleMapsApiKey())
                .build();
    }

    @Override
    public ProfilePhoneDto getProfileInitialsByPhoneNumber(final String phoneNumber) {
        final HttpEntity<String> entity = getEveryoneApiHttpEntity();
        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(appProperties.getCommon().getEveryoneapiUrl() + phoneNumber)
                .queryParam(EVERYONE_API_QUERY_PARAM_NAME, EVERYONE_API_QUERY_PARAM_VALUE);
        try {
            final ResponseEntity<EveryOneApiResponseDto> apiResponse = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, EveryOneApiResponseDto.class);
            final EveryOneApiResponseDto responseBody = apiResponse.getBody();
            if (responseBody != null) {
                final String name = responseBody.getData().getName();
                if (StringUtils.hasText(name)) {
                    return ProfilePhoneDto.builder().initials(name).isExists(Boolean.TRUE).build();
                }
                return ProfilePhoneDto.builder().isExists(Boolean.FALSE).build();
            }
            return ProfilePhoneDto.builder().isExists(Boolean.FALSE).build();
        } catch (RestClientException e) {
            return ProfilePhoneDto.builder().isExists(Boolean.FALSE).build();
        }
    }

    @Override
    public Coordinates getCoordinatesByAddress(final String country, final String city, final String address) {
        final Coordinates coordinates = new Coordinates();
        try {
            final GeocodingResult[] results = GeocodingApi.geocode(context, getAddressForGeocode(country, city, address)).await();
            if (results != null && results.length > 0) {
                final GeocodingResult geocodingResult = results[0];
                coordinates.setLat(String.valueOf(geocodingResult.geometry.location.lat));
                coordinates.setLng(String.valueOf(geocodingResult.geometry.location.lng));
                return coordinates;
            }
        } catch (ApiException  | InterruptedException | IOException e) {
            e.printStackTrace();
            return coordinates;
        }
        return coordinates;
    }

    private HttpEntity<String> getParserHttpEntity(final Object parserRequestDto) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new HttpEntity<>(new ObjectMapper().writeValueAsString(parserRequestDto), headers);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private HttpEntity<String> getEveryoneApiHttpEntity() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth(appProperties.getCommon().getEveryoneapiLogin(), appProperties.getCommon().getEveryoneapiPassword());
        return new HttpEntity<>(headers);
    }

    private static String getAddressForGeocode(final String country, final String city, final String address) {
        return Optional.ofNullable(address).orElse("") + " " +
               Optional.ofNullable(country).orElse("") + " " +
               Optional.ofNullable(city).orElse("") ;
    }
}
