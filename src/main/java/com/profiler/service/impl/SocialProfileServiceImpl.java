package com.profiler.service.impl;

import com.profiler.dto.PageDto;
import com.profiler.dto.search.profile.AutocompleteSearchRequestDto;
import com.profiler.dto.search.profile.SearchRequestDto;
import com.profiler.dto.search.profile.SocialProfileDto;
import com.profiler.exception.ResourceNotFoundException;
import com.profiler.mapper.SocialProfileMapper;
import com.profiler.model.SocialProfile;
import com.profiler.repository.SocialProfileRepository;
import com.profiler.service.ExternalService;
import com.profiler.service.SocialProfileService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SocialProfileServiceImpl implements SocialProfileService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SocialProfileServiceImpl.class);

    private static final String ONLY_NUMBERS_REGEX = "[^0-9]";

    private final SocialProfileRepository socialProfileRepository;
    private final SocialProfileMapper socialProfileMapper;
    private final ExternalService externalService;
    private final ProfileSearchService profileSearchService;
    private final GroupSearchService groupSearchService;

    @Override
    @Transactional
    public SocialProfile add(final SocialProfile socialProfile) {
        LOGGER.info("Saving profile with nickname: {}", socialProfile.getNickname());
        return socialProfileRepository.save(socialProfile);
    }

    @Override
    @Transactional
    public SocialProfile edit(final SocialProfile socialProfile) {
        LOGGER.info("Editing profile with id: {}", socialProfile.getId());
        return add(socialProfile);
    }

    @Override
    @Transactional
    public void delete(final SocialProfile socialProfile) {
        LOGGER.info("Removing profile with id: {}", socialProfile.getId());
        socialProfileRepository.delete(socialProfile);
    }

    @Override
    public SocialProfile get(final Long id) {
        return socialProfileRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("SocialProfile", "id", id));
    }

    @Override
    public List<SocialProfile> getAll() {
        return socialProfileRepository.findAll();
    }


    @Override
    @Transactional
    public PageDto<SocialProfileDto> autocompleteSearch(final AutocompleteSearchRequestDto search) {
        final Page<SocialProfile> searchResult = profileSearchService.autocompleteSearch(search);
        return new PageDto<>(socialProfileMapper.toDtos(searchResult.getContent()), searchResult);
    }

    @Override
    public PageDto<SocialProfileDto> internalSearch(final SearchRequestDto search) {
        final Page<SocialProfile> searchResult = profileSearchService.fuzzySearch(search);
        return new PageDto<>(socialProfileMapper.toDtos(searchResult.getContent()), searchResult);
    }
}
