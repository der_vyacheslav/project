package com.profiler.service;

import java.util.List;

public interface GenericService<T, ID> {
    T add(T obj);

    T edit(T obj);

    void delete(T obj);

    T get(ID id);

    List<T> getAll();
}
