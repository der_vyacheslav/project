package com.profiler.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Resolves request locale by request header
 */
@Configuration
public class CustomLocaleResolver extends AcceptHeaderLocaleResolver implements WebMvcConfigurer {
    private static final List<Locale> SUPPORTED_LOCALES = Collections.singletonList(Locale.ENGLISH);
    private static final String ACCEPT_LANGUAGE_HEADER = "Accept-Language";

    @Override
    public Locale resolveLocale(final HttpServletRequest request) {
        final String headerLang = request.getHeader(ACCEPT_LANGUAGE_HEADER);
        if (StringUtils.hasText(headerLang)) {
            final Locale headerLocale = Locale.lookup(Locale.LanguageRange.parse(headerLang), SUPPORTED_LOCALES);
            if (Objects.nonNull(headerLocale)) {
                return headerLocale;
            }
        }
        return Locale.ENGLISH;
    }
}
