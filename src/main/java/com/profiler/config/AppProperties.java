package com.profiler.config;

import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Bind all configurations prefixed with "app" from application.yaml properties.
 */
@Getter
@ConfigurationProperties(prefix = "app")
public class AppProperties {
    private final Auth auth = new Auth();
    private final OAuth2 oAuth2 = new OAuth2();
    private final Common common = new Common();

    @Getter
    @Setter
    public static final class Auth {
        /** Token secret signing key */
        private String tokenSecret;
        private long tokenExpirationMsec;
        private SignatureAlgorithm signatureAlgorithm;
        private String tokenType;
    }

    public static final class OAuth2 {
        private List<String> authorizedRedirectUris = new ArrayList<>();

        public List<String> getAuthorizedRedirectUris() {
            return authorizedRedirectUris;
        }

        public OAuth2 authorizedRedirectUris(final List<String> authorizedRedirectUris) {
            this.authorizedRedirectUris = authorizedRedirectUris;
            return this;
        }
    }

    @Getter
    @Setter
    public static final class Common {
        private String serverProtocol;
        private String serverName;
        private String serverPort;
        private String frontEndServerDomain;
        private String imageUploadDir;
        private String imageRelativePath;
        private String adminEmail;
        private String telegramBotToken;
        private String telegramBotName;
        private String everyoneapiUrl;
        private String everyoneapiLogin;
        private String everyoneapiPassword;
        private String googleMapsApiKey;
        private String parserProfileUrl;
        private String parserGroupUrl;
        private String parserMessageUrl;
    }
}
