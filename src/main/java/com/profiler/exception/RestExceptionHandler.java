package com.profiler.exception;

import com.profiler.dto.error.ApiError;
import com.profiler.dto.search.profile.SearchCountResponseDto;
import com.profiler.model.enums.ProfileType;
import com.profiler.util.JsonItemFactory;
import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
@RequiredArgsConstructor
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private final MessageSource messageSource;
    private final JsonItemFactory jsonItemFactory;
    protected final Log logger = LogFactory.getLog(this.getClass());

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<Object> handleBadCredentials(final BadCredentialsException ex, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final String message = messageSource.getMessage("error.wrong.authorization.data", null, LocaleContextHolder.getLocale());
        final ApiError apiError = new ApiError(BAD_REQUEST, message, ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(OAuth2AuthenticationProcessingException.class)
    public ResponseEntity<Object> handleOAuth2Request(final OAuth2AuthenticationProcessingException ex,
                                                      final WebRequest request) {
        logger.info(ex.getClass().getName());

        final ApiError apiError = new ApiError(BAD_REQUEST, ex.getLocalizedMessage(), ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<Object> handleBadRequest(final BadRequestException ex, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final String message = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
        final ApiError apiError = new ApiError(BAD_REQUEST, message, ex.getCustomError(), ex);
        return buildResponseEntity(apiError);
    }

    /**
     * Handle MissingServletRequestParameterException. Triggered when a 'required' request parameter is missing.
     *
     * @param ex      MissingServletRequestParameterException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    public ResponseEntity<Object> handleMissingServletRequestParameter(
            final MissingServletRequestParameterException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final String error = ex.getParameterName() +
                messageSource.getMessage("error.param.is.missing", null, LocaleContextHolder.getLocale());
        final ApiError apiError = new ApiError(BAD_REQUEST, error, ex);
        return buildResponseEntity(apiError);
    }

    /**
     * Handle HttpMediaTypeNotSupportedException. This one triggers when JSON is invalid as well.
     *
     * @param ex      HttpMediaTypeNotSupportedException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    public ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            final HttpMediaTypeNotSupportedException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        final String errorMessage =
                messageSource.getMessage("error.media.type.not.supported", null, LocaleContextHolder.getLocale());
        builder.append(" ").append(errorMessage).append(" ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));
        final ApiError apiError = new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, builder.substring(0, builder.length() - 2), ex);
        return buildResponseEntity(apiError);
    }

    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final String message = messageSource.getMessage("error.validation", null, LocaleContextHolder.getLocale());
        final ApiError apiError = new ApiError(BAD_REQUEST, message, ex);
        apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
        apiError.addValidationError(ex.getBindingResult().getGlobalErrors());
        return buildResponseEntity(apiError);
    }

    /**
     * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
     *
     * @param ex      HttpMessageNotReadableException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    public ResponseEntity<Object> handleHttpMessageNotReadable(
            final HttpMessageNotReadableException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final String message = messageSource.getMessage("error.malformed.json.request", null, LocaleContextHolder.getLocale());
        final ApiError apiError = new ApiError(BAD_REQUEST, message, ex);
        return buildResponseEntity(apiError);
    }

    /**
     * Handle NoHandlerFoundException.
     *
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    public ResponseEntity<Object> handleNoHandlerFoundException(
            final NoHandlerFoundException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final String message = String.format("Could not find the %s method for URL %s", ex.getHttpMethod(), ex.getRequestURL());
        final ApiError apiError = new ApiError(BAD_REQUEST, message, ex);
        return buildResponseEntity(apiError);
    }

    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
     *
     * @param ex the ConstraintViolationException
     * @return the ApiError object
     */
    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(
            final javax.validation.ConstraintViolationException ex) {
        logger.info(ex.getClass().getName());

        final String message = messageSource.getMessage("error.validation", null, LocaleContextHolder.getLocale());
        final ApiError apiError = new ApiError(BAD_REQUEST, message, ex);
        apiError.addValidationErrors(ex.getConstraintViolations());
        return buildResponseEntity(apiError);
    }

    @Override
    public ResponseEntity<Object> handleHttpRequestMethodNotSupported(final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());

        final StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));

        final ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, builder.toString(), ex);
        return buildResponseEntity(apiError);
    }

    @Override
    public ResponseEntity<Object> handleExceptionInternal(final Exception ex, final @Nullable Object body,
                                                          final HttpHeaders headers, final HttpStatus status,
                                                          final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.info(body);
        logger.info(ex.getMessage());
        ex.getCause().printStackTrace();
        ex.printStackTrace();
        if (body == null) {
            final List<SearchCountResponseDto> counts = new LinkedList<>();
            EnumSet.allOf(ProfileType.class).forEach(type -> {
                counts.add(SearchCountResponseDto
                        .builder()
                        .type(type)
                        .count(0).build());
            });
            return ResponseEntity.ok(jsonItemFactory.successResult(counts));
        }
        final ApiError apiError = new ApiError(INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), ex);
        return buildResponseEntity(apiError);
    }

    private ResponseEntity<Object> buildResponseEntity(final ApiError apiError) {
        return ResponseEntity.ok(jsonItemFactory.failResult(apiError));
    }
}
