package com.profiler.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ApiExceptionCode {

    //1xx
    COMMON(101, HttpStatus.BAD_REQUEST, "API common exception"),
    INTERNAL_ERROR(102, HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error"),
    VALIDATION_ERROR(103, HttpStatus.UNPROCESSABLE_ENTITY, "There were validation errors"),
    VALIDATION_ERROR_WITH_MSG(103, HttpStatus.UNPROCESSABLE_ENTITY, "There were validation errors: %s"),
    BAD_ARGUMENT(104, HttpStatus.BAD_REQUEST, "Bad argument. $s"),
    INTERNAL_PRECONDITION_FAILED(104, HttpStatus.INTERNAL_SERVER_ERROR, "Internal precondition failed"),
    INTERNAL_ERROR_WITH_MSG(105, HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error: %s"),
    INCORRECT_EDIT(106, HttpStatus.BAD_REQUEST, "Incorrect edit: %s"),

    //3xx
    ENTITY_NOT_FOUND(301, HttpStatus.NOT_FOUND, "Entity not found"),
    ENTITY_NOT_FOUND_WITH_ID(302, HttpStatus.NOT_FOUND, "Entity %s not found with ID=%s"),
    FILE_NOT_FOUND(305, HttpStatus.NOT_FOUND, "File not found"),

    //4xx
    BAD_VALIDATION_REQUEST(400, HttpStatus.BAD_REQUEST, "Validation error"),
    NOT_AUTHENTICATED(401, HttpStatus.UNAUTHORIZED, "Authentication required"),
    PAYMENT_REQUIRED(402, HttpStatus.PAYMENT_REQUIRED, "system.forbidden"),
    FORBIDDEN(403, HttpStatus.FORBIDDEN, "Forbidden"),
    NOT_FOUND(404, HttpStatus.NOT_FOUND, "Not found"),
    NOT_ACCEPTABLE(406, HttpStatus.NOT_ACCEPTABLE, "Not Acceptable"),
    CONFLICT(409, HttpStatus.CONFLICT, "Conflict: %s"),
    LOCKED(423, HttpStatus.LOCKED, "Locked"),
    UNSUPPORTED_MEDIA_TYPE(415, HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"),

    //5xx
    INTERNAL_SERVER_ERROR(500, HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error"),
    DECODER_PROBLEM(501, HttpStatus.INTERNAL_SERVER_ERROR, "Decoder Saving problem"),
    ENCODER_PROBLEM(502, HttpStatus.INTERNAL_SERVER_ERROR, "Encoder Saving problem"),
    SERVICE_UNAVAILABLE(503, HttpStatus.SERVICE_UNAVAILABLE, "Service Unavailable"),
    ;

    private int code;

    private HttpStatus status;

    private String defaultMessage;

    ApiExceptionCode(int code, HttpStatus status, String defaultMessage) {
        this.code = code;
        this.status = status;
        this.defaultMessage = defaultMessage;
    }

    public static ApiExceptionCode fromCode(int code) {
        for (ApiExceptionCode exceptionCode : ApiExceptionCode.values()) {
            if (exceptionCode.code == code) {
                return exceptionCode;
            }
        }
        return null;
    }
}