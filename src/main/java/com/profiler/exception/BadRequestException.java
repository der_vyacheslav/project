package com.profiler.exception;

import com.profiler.model.enums.CustomError;
import lombok.Getter;

@Getter
public class BadRequestException extends RuntimeException {
    private CustomError customError;

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(final String message, final CustomError customError) {
        super(message);
        this.customError = customError;
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
