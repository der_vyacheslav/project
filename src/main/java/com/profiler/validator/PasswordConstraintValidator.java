package com.profiler.validator;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.WhitespaceRule;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * Represents password validator by predefined constraints.
 */
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {
    private static final int MIN_PASSWORD_LENGTH = 8;
    private static final int MAX_PASSWORD_LENGTH = 50;

    @Override
    public void initialize(ValidPassword constraintAnnotation) {

    }

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        final PasswordValidator passwordValidator = new PasswordValidator(Arrays.asList(
              new LengthRule(MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH),
              new CharacterRule(EnglishCharacterData.UpperCase),
              new CharacterRule(EnglishCharacterData.LowerCase),
              new CharacterRule(EnglishCharacterData.Digit),
              new WhitespaceRule()
        ));

        final RuleResult result = passwordValidator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        final String messageTemplate = String.join(",", passwordValidator.getMessages(result));
        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
        return false;
    }
}
