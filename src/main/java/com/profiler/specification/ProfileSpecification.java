package com.profiler.specification;

import com.profiler.model.SocialProfile;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class ProfileSpecification implements Specification<SocialProfile> {
    private List<SearchCriteria> searchCriteriaList;

    public ProfileSpecification(final List<SearchCriteria> searchCriteriaList) {
        this.searchCriteriaList = searchCriteriaList;
    }

    @Override
    public Predicate toPredicate(final Root<SocialProfile> root, final CriteriaQuery<?> criteriaQuery, final CriteriaBuilder builder) {
       return PredicateBuilder.buildPredicates(searchCriteriaList, root, builder);
    }
}
