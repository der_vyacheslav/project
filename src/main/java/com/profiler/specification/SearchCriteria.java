package com.profiler.specification;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class SearchCriteria {
    private final String key;
    private final Object value;
    private final SearchOperation operation;
}
