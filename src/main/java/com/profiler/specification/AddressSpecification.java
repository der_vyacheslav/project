package com.profiler.specification;

import com.profiler.model.Address;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class AddressSpecification implements Specification<Address> {
    private List<SearchCriteria> searchCriteriaList;

    public AddressSpecification(final List<SearchCriteria> searchCriteriaList) {
        this.searchCriteriaList = searchCriteriaList;
    }

    @Override
    public Predicate toPredicate(Root<Address> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return PredicateBuilder.buildPredicates(searchCriteriaList, root, criteriaBuilder);
    }
}
