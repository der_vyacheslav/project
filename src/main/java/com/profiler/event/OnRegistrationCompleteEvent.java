package com.profiler.event;

import com.profiler.dto.UserDto;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

/**
 * Event which have to be published on user's successfully registration
 */
@Getter
public class OnRegistrationCompleteEvent extends ApplicationEvent {
    private final Locale locale;
    private final UserDto user;

    public OnRegistrationCompleteEvent(final UserDto user, final Locale locale) {
        super(user);
        this.user = user;
        this.locale = locale;
    }
}
