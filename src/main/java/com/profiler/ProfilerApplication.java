package com.profiler;

import com.profiler.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
@EnableScheduling
public class ProfilerApplication {

	public static void main(String[] args) {
		//Initialize telegram bot context
		ApiContextInitializer.init();

		SpringApplication.run(ProfilerApplication.class, args);
	}
}

