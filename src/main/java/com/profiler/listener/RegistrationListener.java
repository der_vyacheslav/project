package com.profiler.listener;

import com.profiler.dto.UserDto;
import com.profiler.event.OnRegistrationCompleteEvent;
import com.profiler.model.enums.ConfirmationTokenType;
import com.profiler.service.EmailService;
import com.profiler.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Handling {@link OnRegistrationCompleteEvent}
 */
@Component
@RequiredArgsConstructor
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
    private static final String REGISTRATION_CONFIRM_EMAIL_TEMPLATE = "registrationConfirmEmail";

    private final UserService userService;
    private final EmailService emailService;

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnRegistrationCompleteEvent event) {
        final UserDto user = event.getUser();
        final String confirmationToken = userService.createConfirmationToken(user, ConfirmationTokenType.EMAIL);

        emailService.sendConfirmationEmail(user.getEmail(), confirmationToken, REGISTRATION_CONFIRM_EMAIL_TEMPLATE);
    }
}
