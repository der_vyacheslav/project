package com.profiler.repository;

import com.profiler.model.Group;
import com.profiler.model.SocialProfile;
import com.profiler.model.enums.ParsingStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SocialProfileRepository extends JpaRepository<SocialProfile, Long>, JpaSpecificationExecutor<SocialProfile> {

    void deleteByIdIn(List<Long> ids);

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("UPDATE SocialProfile p SET p.isTracked = TRUE WHERE p.id IN :ids")
    void updateTrackedByIdIn(@Param("ids") List<Long> ids);

    SocialProfile findByNickname(String nickname);

    List<SocialProfile> findByNicknameIn(List<String> nicknames);

    List<SocialProfile> findByExternalUserIdIn(List<Long> externalUserIds);

    List<SocialProfile> findByIsBackground(Boolean isBackground);

    @Query("SELECT COUNT(p) FROM SocialProfile p WHERE :group MEMBER OF p.groups")
    long countSocialProfilesByGroup(@Param("group") Group group);

    @Query("SELECT p.id FROM SocialProfile p WHERE :group MEMBER OF p.groups")
    List<Long> findSocialProfilesIdsByGroup(@Param("group") Group group);

    @Query("SELECT COUNT(p) FROM SocialProfile p " +
           "WHERE :group MEMBER OF p.groups AND p.parsingStatus IN :parsingStatuses")
    long countParsedSocialProfilesByGroup(@Param("group") Group group,
                                          @Param("parsingStatuses") Set<ParsingStatus> parsingStatuses);
}
