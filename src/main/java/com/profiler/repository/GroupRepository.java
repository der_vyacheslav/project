package com.profiler.repository;

import com.profiler.model.Group;
import com.profiler.model.enums.ParsingStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

    List<Group> findByIdInAndParsingStatusIn(List<Long> ids, Set<ParsingStatus> parsingStatuses);

    @Modifying
    @Query("UPDATE Group g SET g.unfreezeSendDateTime = :unfreezeSendDateTime WHERE g.id = :id")
    void updateUnfreezeSendDateTime(@Param("unfreezeSendDateTime") LocalDateTime unfreezeSendDateTime,
                                    @Param("id") Long id);
}
